//arquivo base sugerido pelo professor Akira.
//https://gist.github.com/creationix/707146

// Carrega a biblioteca TCP
net = require('net');

// Array de clientes do bate-papo
var clients = [];

// Inicia o server 
net.createServer(function (socket) {

  // Identifica o cliente
  socket.name = socket.remoteAddress + ":" + socket.remotePort 

  // Carrega o cliente na lista
  clients.push(socket);

  // Envia mensagem de boas vindas ao cliente
  socket.write("Bem vindo a chat " + socket.name + "\n");
  broadcast(socket.name + " joined the chat\n", socket);

  // Mensagens recebidas dos clientes
  socket.on('data', function (data) {
    broadcast(socket.name + "> " + data, socket);
  });

  // Remove o cliente ao sair
  socket.on('end', function () {
    clients.splice(clients.indexOf(socket), 1);
    broadcast(socket.name + " left the chat.\n");
  });
  
  // Envia mensagem a todos os clientes
  function broadcast(message, sender) {
    clients.forEach(function (client) {
      // Don't want to send it to sender
      if (client === sender) return;
      client.write(message);
    });
    // Registra mensagem no servidor
    process.stdout.write(message)
  }

}).listen(5000); //porta do servidor

// aviso de servidor em execução.
console.log("O servidor do chat está sendo executa na porta 5000\n");